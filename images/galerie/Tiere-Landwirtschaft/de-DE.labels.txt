01-hofladen-eibel-st.jpg|Unsere Stallungen|<b>Unsere Stallungen</b><br />Das "Wohnzimmer" eines Landwirts
02-hofladen-eibel-st.jpg|Traktorfahren |<b>Traktorfahren </b><br />Ein Kindertraum...mit viel Arbeit
03-hofladen-eibel-st.jpg|Sags mir ins Ohr|<b>Sags mir ins Ohr</b><br />Hier gibts auch mal ein Busserl
04-hofladen-eibel-st.jpg|Passion Landwirt|<b>Passion Landwirt</b><br />Selbstverständliches Leben mit Tieren
05-hofladen-eibel-st.jpg|Wie gehts dir?|<b>Wie gehts dir heute?</b><br />Gehts den Tieren gut, gehts uns gut.
06-hofladen-eibel-st.jpg|Die Gänse-Gang|<b>Die Gänse-Gang</b><br />Auf gehts ins Freie
07-hofladen-eibel-st.jpg|Besonderer Platz|<b>Besonderer Platz</b><br />Für gute Tiernahrung. Ruhig und lichtdurchflutet
08-hofladen-eibel-st.jpg|Ohne Worte...|<b>Ohne Worte...</b><br />Einfach erleben
