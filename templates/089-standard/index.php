<?php
/**
 * @author   	cg@089webdesign.de
 */
 

defined('_JEXEC') or die;
//include system
include_once(JPATH_ROOT . "/templates/" . $this->template . '/lib/system.php');
//include template Functions CG
include_once(JPATH_ROOT . "/templates/" . $this->template . '/template_functions.php');

//mobioe detect varibalen:
if (!$detect->isMobile()) {
	$detectAgent = "desktop ";
	$clientMobile = false; 
}elseif (!$detect->isMobile() || $detect->isTablet()) {
	$detectAgent = "tablet ";
	$clientMobile = true; 
}else {
	$detectAgent = "phone ";
	$clientMobile = true; 
}
?>
<!DOCTYPE html>
<html>
<head>
	<?php 
	// including head
	include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/head.php');
	?>
</head>

<body id="body" class="site <?php echo $detectAgent . $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. $body_class; print ($clientMobile) ? "mobile " : " ";
?>">

	<!-- Body -->
		<div class="fullwidth site_wrapper">
			<div class="ribbon-wrap right-edge fork lblue">
				<span>
					<a class="typo" href="/kontakt.html" title="Kontaktformular">KONTAKT</a>
					<a class="env_open" href="/kontakt.html" title="Kontaktformular"><img src="/images/env_open.png" alt="Nachricht senden" /></a>
				</span>
			</div>
			<?php if ($currentMenuID == 111 ) : ?> 
				<div id="trenner"></div>
			<?php endif; ?>
			<?php			
			
			// including slider
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/slider.php');			

			// including header
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/menu.php');

			// including top
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/breadcrumbs.php');				
					
			// including top
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/top.php');	
			
			// including top2
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/top2.php');	
									
			// including content
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/content.php');	

			// including bottom
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom.php');	
			
			// including bottom2
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom2.php');
			
			// including content
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/footer.php');				
			
			?>

		</div>
	
	
	<jdoc:include type="modules" name="debug" style="none" />

<script type="text/javascript">

	jQuery(document).ready(function() {
		 jQuery(window).scroll(function(){
			if (jQuery(window).scrollTop() > <?php print ($detect->isMobile()) ? "400" : "850"; ?> ) {
			   	jQuery('.modContent').addClass( "fdIn");
			   		setTimeout(function() {
			   			jQuery('.picHolder').addClass( "shadow");
			   	}, 800);
			} else {
			    //jQuery('.modContent').removeClass("fdIn");
			}		
		});
<?php if ($detect->isMobile()) : ?>
		jQuery.extend(jQuery.lazyLoadXT, {
			  edgeY:  100,
			  srcAttr: 'data-src'
			});
		<?php else : ?>
			jQuery.extend(jQuery.lazyLoadXT, {
				  edgeY:  200,
			  	srcAttr: 'data-src'
			});
		<?php endif;  ?>
	});
</script>
</body>
</html>
