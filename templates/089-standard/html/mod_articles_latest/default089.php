<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_latest mod by cg@089webdesign
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="custom">
<div class="picHolder"><a href="/hofladen-eibel-puchheim-empfehlungen.html" title="Empfehlungen"><img alt="Unsere Empfehlungen" data-src="/images/content/empfehlungen.png" /></a></div>
	<div class="modContent">
		<h3>Aktuelle Empfehlungen</h3>	
		<?php foreach ($list as $item) :  ?>
			<a href="<?php echo $item->link; ?>" itemprop="url">
				<h4>
					<?php echo $item->title; ?>
				</h4>
			</a>					
				<?php print $item->introtext; ?>								
		<?php endforeach; ?>	
	</div>
</div>	
