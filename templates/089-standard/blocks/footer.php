<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<footer class="footer" role="contentinfo">
	<div class="footer-wrap">				
		<div class="innerwidth">
			<img class="footerImg pull-right" data-src="/images/content/logo/bauernhof-einkauf.jpg" alt="Einkaufen auf dem Bauernhof" />	
		</div>	
	</div>
</footer>
<div id="copyright" class="innerwidth"><p>&copy  <?php echo date(Y);?>  Eibel Hofladen - Familie Maier | 
	<a class="imprLink" href="impressum.html" title="Impressum Eibel Hofladen">Impressum</a> | <a class="imprLink" href="/datenschutz.html" title="Datenschutz Eibel Hofladen">Datenschutz</a><br />
	Ihr freundlicher Hofladen in Puchheim bei M&uumlnchen - Einkaufen auf dem Bauernhof - Bayerische Hofmetzgerei mit Tradition</p>
</div>
	
		