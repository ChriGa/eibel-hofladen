<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$poss = array('top1','top2','top3','top4','top5','top6');
$n = 0;
if ($this->countModules('top1')) $n++;
if ($this->countModules('top2')) $n++;
if ($this->countModules('top3')) $n++;
if ($this->countModules('top4')) $n++;
if ($this->countModules('top5')) $n++;
if ($this->countModules('top6')) $n++;

if ($n > 0) {
$span = 12/$n;
?>
<div class="clear-top">
	<div class="container">
		<div class="row-fluid">
			<?php foreach ($poss as $i => $pos): ?>
				<?php if ($this->countModules($pos)) : ?>
				<div class="span<?php echo $span; ?> module_top position_<?php echo $pos; ?>">
					<jdoc:include type="modules" name="<?php echo $pos ?>" style="xhtml" />
				</div>
				<?php endif ?>
			<?php endforeach ?>
		</div> 
	</div> 
</div>	
<?php } ?>	
