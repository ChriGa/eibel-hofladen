<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;
?>
	<nav class="navbar-wrapper">
        <div class="navbar innerwidth">
          <div class="navbar-inner">
            <button type="button" class="btn btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>                                   
			  <?php if ($this->countModules('menu')) : ?>
				<div class="nav-collapse collapse "  role="navigation">
            <?php if(!$detect->isMobile() || $detect->isTablet()) : ?>
					     <jdoc:include type="modules" name="menu" style="custom" />
            <?php else : ?>
                <jdoc:include type="modules" name="menuMobile" style="custom" />
            <?php endif; ?>
				</div>
				<?php endif; ?>
          </div>
        </div>
    </nav>
