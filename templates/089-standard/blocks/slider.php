<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;
?>
<?php if(!$detect->isMobile() || $detect->isTablet() ) : ?>
	<div id="logoContainer">
		<div class="flexbox">
			<a class="" href="/" title="Eibel-Hofladen Metzgerei">
				<img class="logoDesktop" src="/images/content/logo/eibel-logo.png" alt="Logo der Eibel-Hofladen Metzgerei" />
			</a>
			<div id="logoHeader" class="margHeader">
				<h2>Ihre Bayerische Hofmetzgerei</h2>					
			</div>
			<div class="vcard margHeader">
				<p class="tel "><a class="corpColor" href="tel:+498920075515">Tel: 089 - 200 755 15</a></p>
				<p class="adr"><span class="street-address">Dorfstr. 27</span><br />
					<span class="postal-code">82178 </span><span class="region">Puchheim-Ort</span>
				</p>
			</div>
			<div class="eibelIcon ">
				<img src="/images/content/logo/eibel-icon.png" alt="Icon der Eibel-Hofladen Metzgerei" />
			</div>
		</div>
	</div>
	<?php if ($this->countModules('slider')) : ?>
		<div class="clear-slider">
			<div class="ekBHof">
				<img class="" src="/images/content/logo/bauernhof-einkauf.jpg" alt="Einkaufen auf dem Bauernhof" />
			</div>		
				<div class="clear-slider-wrap" id="sliderWrapper">
					<jdoc:include type="modules" name="slider" style="none" />	
				</div>
	<?php endif; ?>
		</div>			
	<?php else : ?>
	<div class="clear-slider">
		<div id="logoContainer" class="mobile">
			<a href="/" alt="Startseite Eibel-Hofladen" title="Eibel-Hofladen Metzgerei">
				<img class="logoDesktop" src="/images/content/logo/eibel-logo.png" alt="Logo der Eibel-Hofladen Metzgerei" />
			</a>
			<div class="eibelIcon ">
				<img src="/images/content/logo/eibel-icon.png" alt="Icon der Eibel-Hofladen Metzgerei" />
			</div>
		</div>
		<div class="vcard">
			<p class="tel "><a class="corpColor" href="tel:+498920075515">Tel: 089 - 200 755 15</a></p>
			<p class="adr"><span class="street-address">Dorfstr. 27</span><br />
				<span class="postal-code">82178 </span><span class="region">Puchheim-Ort</span>
			</p>
		</div>
		<div class="ekBHof">
			<img class="" src="/images/content/logo/bauernhof-einkauf.jpg" alt="Einkaufen auf dem Bauernhof" />
		</div>	
	</div>
<?php endif; ?>